var map = new AMap.Map("mapContainer");
// map.setCity("青岛市"); //用来定位那个城市
var x = 120.11934041976929; //经度
var y = 36.001618315221194; //纬度
var position = new AMap.LngLat(x, y); // 标准写法
map.setCenter(position);
map.setZoom(15);

map.plugin(["AMap.ToolBar"], function() {
  var tool = new AMap.ToolBar();
  map.addControl(tool);
});

// map.plugin(["AMap.OverView"], function() {
//   var view = new AMap.OverView();
//   //因为在LBS中鹰眼视图工具默认为BOOL:False,所以在测试时可以用open函数来让它默认为打开；
//   // view.open();
//   map.addControl(view);
// });

// 添加与删除标记
var people_markers = [];
var guard_markers = [];

function add_people_marker(x = x, y = y) {
  // 将创建的点标记添加到已有的地图实例：
  var marker = new AMap.Marker({
    map: map,
    position: new AMap.LngLat(x, y),
    // offset: new AMap.Pixel(-10, -10),
    icon: new AMap.Icon({
      image: "../Beta0.4/images/icon01_temp.png",
      size: new AMap.Size(20, 25), //图标大小
      imageSize: new AMap.Size(20, 25)
    }), // 添加 Icon 图标 URL
    title: "SDUST"
  });
  // marker.content = 'hhh\\ i\'am first';
  // 打开浮动窗口,如何在点击后再打开
  // 多次点击打开窗口会不会造成一个标记对应多个监听时间?可能会是个bug
  marker.on("click", open_video);
  // 地图上标记完后存入队列
  people_markers.push(marker);
  // AMap.event.addListener(people_marker, 'click', open_video('video'));
}
function remove_people_marker() {
  map.remove(people_markers.pop());
}

function add_guard_marker(x = x, y = y) {
  var guard_marker = new AMap.Marker({
    map: map,
    position: new AMap.LngLat(x, y),
    icon: new AMap.Icon({
      image: "../Beta0.4/images/icon02_temp.png",
      size: new AMap.Size(35, 50), //图标大小
      imageSize: new AMap.Size(35, 50) //宽、高
    }), // 添加 Icon 图标 URL
    title: "青岛"
  });
  guard_markers.push(guard_marker);
}

function remove_guard_marker() {
  map.remove(guard_markers.pop());
}

function open_video(e) {
  var popUp = document.getElementById("video");
  popUp.style.visibility = "visible";
}

// 禁止地图滚动的时候滚动条滚动
// function forbidScroll() {}

// $(document).ready(function(){
// 	$("#mapContainer").scroll(function(event){
// 	//   event.preventDefault();
// 	console.log("检测到滚动!!!");
// 	});
//   });
// document.getElementById("mapContainer111").onmousewheel = function(e) {
// 	e = e || window.event;
//     if (e.preventDefault)
//         e.preventDefault();
//     e.returnValue = false;
// };
